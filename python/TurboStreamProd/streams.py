from TurboStreamProd import (
    streams_2015,
    streams_2016,
    streams_2017,
    streams_5tev2017,
    streams_2018
)

turbo_streams = {
    '2015': streams_2015.turbo_streams,
    '2016': streams_2016.turbo_streams,
    '2017': streams_2017.turbo_streams,
    '5tev2017': streams_5tev2017.turbo_streams,
    '2018': streams_2018.turbo_streams
}
turcal_streams = {
    '2015': streams_2015.turcal_streams,
    '2016': streams_2016.turcal_streams,
    '2017': streams_2017.turcal_streams,
    '5tev2017': streams_5tev2017.turcal_streams,
    '2018': streams_2018.turcal_streams
}
