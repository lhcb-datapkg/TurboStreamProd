# Functions to get list of Hlt2 lines to process
from __future__ import print_function

def allLines(prodDict,version,debug=False):
    retList=[]
    for key in prodDict[version]:
        retList+=prodDict[version][key]
    if debug:
        print("Tesla will process the following lines: "+str(retList))
    return retList

def streamLines(prodDict,version,stream,debug=False):
    retList=[]
    retList+=prodDict[version][stream]
    if debug:
        print("Tesla will process the following lines: "+str(retList))
    return retList
