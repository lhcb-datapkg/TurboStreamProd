# Dictionary of Turbo stream production versions
# and list of Turbo lines in their given streams

prodDict = { 
        'smog':  
        { 
            'Commissioning' : [
                'Hlt2KS0_LLTurbo'
                ,'Hlt2KS0_DDTurbo'
                ]
        },
        '2016_0x21331609_additions' : 
        {
            'TurCalAdditions' : [ 
                'Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib',
                'Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib',
                'Hlt2PIDDs2PiPhiMuMuPosTaggedTurboCalib',
                'Hlt2PIDDs2PiPhiMuMuNegTaggedTurboCalib'
                ],
            'CharmHad_additions' : [ 
                'Hlt2SLB_B2D0Mu_D02KmKpTurbo',
                'Hlt2SLB_B2D0Mu_D02KmPipTurbo',
                'Hlt2SLB_B2D0Mu_D02PimPipTurbo',
                'Hlt2SLB_B2DstMu_D02KmKpTurbo',
                'Hlt2SLB_B2DstMu_D02KmPipTurbo',
                'Hlt2SLB_B2DstMu_D02PimPipTurbo',
                ],
            'OniaBeautyStrange_additions' : [ 
                'Hlt2ExoticaRHNuTurbo',
                'Hlt2LFVPhiMuETurbo',
                'Hlt2LFVPromptPhiMuETurbo',
                'Hlt2LFVUpsilonMuETurbo',
                'Hlt2PhiPhi2EETurbo',
                'Hlt2PhiPromptPhi2EETurbo'
                ]
        },
        '2016_0x21271600' : 
        {
            'TurCalAdditions' : [ 
                'Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib',
                'Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib'
              ],
            'PID' : [ 
                'Hlt2PIDB2KJPsiEENegTaggedTurboCalib',
                'Hlt2PIDB2KJPsiEEPosTaggedTurboCalib', 
                'Hlt2PIDB2KJPsiMuMuNegTaggedTurboCalib',
                'Hlt2PIDB2KJPsiMuMuPosTaggedTurboCalib',
                'Hlt2PIDD02KPiTagTurboCalib',
                'Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib',
                'Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib',
                'Hlt2PIDDs2KKPiSSTaggedTurboCalib',
                'Hlt2PIDDs2MuMuPiNegTaggedTurboCalib',
                'Hlt2PIDDs2MuMuPiPosTaggedTurboCalib',
                'Hlt2PIDKs2PiPiLLTurboCalib',
                'Hlt2PIDLambda2PPiLLTurboCalib',
                'Hlt2PIDLambda2PPiLLhighPTTurboCalib',
                'Hlt2PIDLambda2PPiLLisMuonTurboCalib',
                'Hlt2PIDLambda2PPiLLveryhighPTTurboCalib',
                'Hlt2PIDLb2LcMuNuTurboCalib',
                'Hlt2PIDLb2LcPiTurboCalib',
                'Hlt2PIDLc2KPPiTurboCalib'
              ],
            'TrackEff' : [ 
                'Hlt2TrackEffDiMuonDownstreamMinusHighStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamMinusHighStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamMinusLowStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamMinusLowStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamPlusHighStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamPlusHighStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamPlusLowStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonDownstreamPlusLowStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTMinusHighStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTMinusHighStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTMinusLowStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTMinusLowStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTPlusHighStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTPlusHighStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTPlusLowStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonMuonTTPlusLowStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonMinusHighStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonMinusHighStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonMinusLowStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonMinusLowStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonPlusHighStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonPlusHighStatTaggedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonPlusLowStatMatchedTurboCalib',
                'Hlt2TrackEffDiMuonVeloMuonPlusLowStatTaggedTurboCalib'
                ],
            'CharmHad' : [ 
                'Hlt2CharmHadD02KmPipTurbo',
                'Hlt2CharmHadDp2KS0KS0KpTurbo',
                'Hlt2CharmHadDp2KS0KS0PipTurbo',
                'Hlt2CharmHadDp2KS0KmKpPip_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0KmKpPip_KS0LLTurbo',
                'Hlt2CharmHadDp2KS0KmPipPip_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0KmPipPip_KS0LLTurbo',
                'Hlt2CharmHadDp2KS0KpKpPim_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0KpKpPim_KS0LLTurbo',
                'Hlt2CharmHadDp2KS0KpPimPip_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0KpPimPip_KS0LLTurbo',
                'Hlt2CharmHadDp2KS0Kp_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0Kp_KS0LLTurbo',
                'Hlt2CharmHadDp2KS0PimPipPip_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0PimPipPip_KS0LLTurbo',
                'Hlt2CharmHadDp2KS0Pip_KS0DDTurbo',
                'Hlt2CharmHadDp2KS0Pip_KS0LLTurbo',
                'Hlt2CharmHadDp2KmKmKpPipPipTurbo',
                'Hlt2CharmHadDp2KmKpPimPipPipTurbo',
                'Hlt2CharmHadDp2KmPimPipPipPipTurbo',
                'Hlt2CharmHadDpToKmKpKpTurbo',
                'Hlt2CharmHadDpToKmKpPipTurbo',
                'Hlt2CharmHadDpToKmPipPipTurbo',
                'Hlt2CharmHadDpToKmPipPip_ForKPiAsymTurbo',
                'Hlt2CharmHadDpToKmPipPip_LTUNBTurbo',
                'Hlt2CharmHadDpToKpKpPimTurbo',
                'Hlt2CharmHadDpToKpPimPipTurbo',
                'Hlt2CharmHadDpToPimPipPipTurbo',
                'Hlt2CharmHadDsp2KS0KS0KpTurbo',
                'Hlt2CharmHadDsp2KS0KS0PipTurbo',
                'Hlt2CharmHadDsp2KS0KmKpPip_KS0DDTurbo',
                'Hlt2CharmHadDsp2KS0KmKpPip_KS0LLTurbo',
                'Hlt2CharmHadDsp2KS0KmPipPip_KS0DDTurbo',
                'Hlt2CharmHadDsp2KS0KmPipPip_KS0LLTurbo',
                'Hlt2CharmHadDsp2KS0KpKpPim_KS0DDTurbo',
                'Hlt2CharmHadDsp2KS0KpKpPim_KS0LLTurbo',
                'Hlt2CharmHadDsp2KS0KpPimPip_KS0DDTurbo',
                'Hlt2CharmHadDsp2KS0KpPimPip_KS0LLTurbo',
                'Hlt2CharmHadDsp2KS0PimPipPip_KS0DDTurbo',
                'Hlt2CharmHadDsp2KS0PimPipPip_KS0LLTurbo',
                'Hlt2CharmHadDsp2KmKmKpPipPipTurbo',
                'Hlt2CharmHadDsp2KmKpPimPipPipTurbo',
                'Hlt2CharmHadDsp2KmPimPipPipPipTurbo',
                'Hlt2CharmHadDspToKmKpKpTurbo',
                'Hlt2CharmHadDspToKmKpPipTurbo',
                'Hlt2CharmHadDspToKmKpPip_LTUNBTurbo',
                'Hlt2CharmHadDspToKmPipPipTurbo',
                'Hlt2CharmHadDspToKpKpPimTurbo',
                'Hlt2CharmHadDspToKpPimPipTurbo',
                'Hlt2CharmHadDspToPimPipPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DDTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DD_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LLTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LL_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DDTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DD_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LLTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LL_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DDTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DD_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LLTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LL_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DDTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LLTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LL_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmKmKpPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmKpKpPimTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmKpPimPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmKpTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmPimPipPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KpPimPimPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KpPimTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02KpPim_LTUNBTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02PimPimPipPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02PimPipTurbo',
                'Hlt2CharmHadDstp2D0Pip_D02PimPip_LTUNBTurbo',
                'Hlt2CharmHadLcp2KS0KS0PpTurbo',
                'Hlt2CharmHadLcp2LamKmKpPip_Lam2PpPimTurbo',
                'Hlt2CharmHadLcp2LamKmPipPip_Lam2PpPimTurbo',
                'Hlt2CharmHadLcp2LamKp_LamDDTurbo',
                'Hlt2CharmHadLcp2LamKp_LamLLTurbo',
                'Hlt2CharmHadLcp2LamPip_LamDDTurbo',
                'Hlt2CharmHadLcp2LamPip_LamLLTurbo',
                'Hlt2CharmHadLcpToPpKmKmPipPipTurbo',
                'Hlt2CharmHadLcpToPpKmKpPimPipTurbo',
                'Hlt2CharmHadLcpToPpKmKpTurbo',
                'Hlt2CharmHadLcpToPpKmPimPipPipTurbo',
                'Hlt2CharmHadLcpToPpKmPipTurbo',
                'Hlt2CharmHadLcpToPpKmPip_LTUNBTurbo',
                'Hlt2CharmHadLcpToPpKpKpPimPimTurbo',
                'Hlt2CharmHadLcpToPpKpPimPimPipTurbo',
                'Hlt2CharmHadLcpToPpKpPimTurbo',
                'Hlt2CharmHadLcpToPpPimPimPipPipTurbo',
                'Hlt2CharmHadLcpToPpPimPipTurbo',
                'Hlt2CharmHadOmm2LamKm_DDDTurbo',
                'Hlt2CharmHadOmm2LamKm_DDLTurbo',
                'Hlt2CharmHadOmm2LamKm_LLLTurbo',
                'Hlt2CharmHadPentaToPhiPpPimTurbo',
                'Hlt2CharmHadXic0ToPpKmKmPipTurbo',
                'Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurbo',
                'Hlt2CharmHadXiccp2D0PpKmPim_D02KmPipTurbo',
                'Hlt2CharmHadXiccp2D0PpKmPip_D02KmPipTurbo',
                'Hlt2CharmHadXiccp2D0PpKpPim_D02KmPipTurbo',
                'Hlt2CharmHadXiccp2DpPpKm_Dp2KmPipPipTurbo',
                'Hlt2CharmHadXiccp2DpPpKp_Dp2KmPipPipTurbo',
                'Hlt2CharmHadXiccp2LcpKmPim_Lcp2PpKmPipTurbo',
                'Hlt2CharmHadXiccp2LcpKmPip_Lcp2PpKmPipTurbo',
                'Hlt2CharmHadXiccp2LcpKpPim_Lcp2PpKmPipTurbo',
                'Hlt2CharmHadXiccp2Xic0Pim_Xic0ToPpKmKmPipTurbo',
                'Hlt2CharmHadXiccp2Xic0Pip_Xic0ToPpKmKmPipTurbo',
                'Hlt2CharmHadXiccp2XicpPimPim_Xicp2PpKmPipTurbo',
                'Hlt2CharmHadXiccp2XicpPimPip_Xicp2PpKmPipTurbo',
                'Hlt2CharmHadXiccpp2D0PpKmPimPip_D02KmPipTurbo',
                'Hlt2CharmHadXiccpp2D0PpKmPipPip_D02KmPipTurbo',
                'Hlt2CharmHadXiccpp2D0PpKpPimPip_D02KmPipTurbo',
                'Hlt2CharmHadXiccpp2DpPpKmPim_Dp2KmPipPipTurbo',
                'Hlt2CharmHadXiccpp2DpPpKmPip_Dp2KmPipPipTurbo',
                'Hlt2CharmHadXiccpp2DpPpKpPip_Dp2KmPipPipTurbo',
                'Hlt2CharmHadXiccpp2LcpKmPimPip_Lcp2PpKmPipTurbo',
                'Hlt2CharmHadXiccpp2LcpKmPipPip_Lcp2PpKmPipTurbo',
                'Hlt2CharmHadXiccpp2LcpKpPimPip_Lcp2PpKmPipTurbo',
                'Hlt2CharmHadXiccpp2Xic0PimPip_Xic0ToPpKmKmPipTurbo',
                'Hlt2CharmHadXiccpp2Xic0PipPip_Xic0ToPpKmKmPipTurbo',
                'Hlt2CharmHadXiccpp2XicpPim_Xicp2PpKmPipTurbo',
                'Hlt2CharmHadXiccpp2XicpPip_Xicp2PpKmPipTurbo',
                'Hlt2CharmHadXicpToPpKmPipTurbo',
                'Hlt2CharmHadXim2LamPim_DDDTurbo',
                'Hlt2CharmHadXim2LamPim_DDLTurbo',
                'Hlt2CharmHadXim2LamPim_LLLTurbo',
                'Hlt2TrackEff_D0ToKpiKaonProbeTurbo',
                'Hlt2TrackEff_D0ToKpiPionProbeTurbo'
              ],
            'OniaBeautyStrange' : [ 
                'Hlt2BottomoniumDiKstarTurbo',
                'Hlt2DiMuonBTurbo',
                'Hlt2DiMuonJPsiTurbo',
                'Hlt2DiMuonPsi2SLowPTTurbo',
                'Hlt2DiMuonPsi2STurbo',
                'Hlt2ExoticaPrmptDiMuonSSTurbo',
                'Hlt2ExoticaPrmptDiMuonTurbo',
                'Hlt2LFVJpsiMuETurbo',
                'Hlt2StrangeKPiPiPiTurbo'
              ]
        },
        'v10r0_0x00fa0051' : 
        {
            'PID' : [ 'Hlt2PID' + k + 'TurboCalib' for k in [
              'DetPhiKKUnbiased',
              'B2KJPsiMuMuPosTagged',
              'B2KJPsiPPNegTagged',
              'DetJPsiMuMuNegTagged',
              'Lambda2PPiDD',
              'Ks2PiPiLL',
              'Ds2PiPhiKKUnbiased',
              'Lb2LcMuNu',
              'Lambda2PPiLLveryhighPT',
              'Ds2PiPhiKKNegTagged',
              'DetPhiMuMuNegTagged',
              'Lambda2PPiLL',
              'B2KJPsiEENegTagged',
              'Lambda2PPiDDhighPT',
              'Lb2LcPi',
              'B2KJPsiEEL0NegTagged',
              'Lambda2PPiDDisMuon',
              'DetJPsiMuMuPosTagged',
              'DetJPsiEEL0NegTagged',
              'Lambda2PPiLLisMuon',
              'B2KJPsiEEPosTagged',
              'B2KJPsiPPPosTagged',
              'Ks2PiPiDD',
              'D02KPiTag',
              'Ds2PiPhiMuMuPosTagged',
              'B2KJPsiEEL0PosTagged',
              'DetJPsiEEL0PosTagged',
              'DetPhiKKNegTagged',
              'Lambda2PPiDDveryhighPT',
              'Ds2PiPhiKKPosTagged',
              'Scpp2LcPi',
              'DetJPsiEENegTagged',
              'DetJPsiPPPosTagged',
              'D02KPiPiPiTag',
              'D02KPiPiPi',
              'DetPhiMuMuPosTagged',
              'DetJPsiPPNegTagged',
              'DetPhiKKPosTagged',
              'Ds2PiPhiMuMuNegTagged',
              'Lambda2PPiLLhighPT',
              'DetJPsiEEPosTagged',
              'B2KJPsiMuMuNegTagged',
              'Sc02LcPi']
              ]
            ,'TrackCalib' : [
                'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalib'
                ]
            ,'DiMuon' : [ 
                'Hlt2DiMuonJPsiTurbo'
                ,'Hlt2JPsiReFitPVsTurbo'
                ,'Hlt2DiMuonPsi2STurbo'
                ,'Hlt2DiMuonBTurbo'
                ]
            ,'Charm' : [ 'Hlt2CharmHad' + k + 'Turbo' for k in [
                'Dstp2D0Pip_D02PimPip'
                ,'Dstp2D0Pip_D02KpPim'
                ,'Dstp2D0Pip_D02KmKp'
                ,'LcpToPpPimPip'
                ,'LcpToPpKmPip'
                ,'Lcp2LamKp_LamLL'
                ,'Dsp2KS0PimPipPip_KS0DD'
                ,'DpToKmKpPip'
                ,'Dp2KS0PimPipPip_KS0DD'
                ,'Xic0ToPpKmKmPip_LTUNB'
                ,'Xic0ToPpKmKmPip'
                ,'PentaToPhiPpPim'
                ,'MuTag_DstpMu_Dstp2D0Pip_D02KmPip'
                ,'MuTag_DstpMu_Dstp2D0Pip_D02KmKp'
                ,'LcpToPpKmPip_LTUNB'
                ,'LcpToPpKmKpPimPip'
                ,'Lcp2LamPip_LamLL'
                ,'Lcp2LamKmPipPip_Lam2PpPim'
                ,'Lcp2LamKmKpPip_Lam2PpPim'
                ,'Dstp2D0Pip_D02KmKpKpPim'
                ,'Dstp2D0Pip_D02KS0KS0_KS0LL_KS0DD'
                ,'Dstp2D0Pip_D02KS0KS0_KS0LL'
                ,'Dstp2D0Pip_D02KS0KS0_KS0DD'
                ,'DspToKmKpPip_LTUNB'
                ,'DspToKmKpKp'
                ,'Dsp2KmKpPimPipPip'
                ,'Dsp2KmKmKpPipPip'
                ,'Dsp2KS0KpPimPip_KS0DD'
                ,'Dsp2KS0KpKpPim_KS0LL'
                ,'Dsp2KS0KpKpPim_KS0DD'
                ,'Dsp2KS0KmPipPip_KS0LL'
                ,'Dsp2KS0KmKpPip_KS0LL'
                ,'Dsp2KS0KmKpPip_KS0DD'
                ,'DpToKmPipPip_LTUNB'
                ,'DpToKmKpKp'
                ,'Dp2KmPimPipPipPip'
                ,'Dp2KmKpPimPipPip'
                ,'Dp2KmKmKpPipPip'
                ,'Dp2KS0Kp_KS0LL'
                ,'Dp2KS0KpPimPip_KS0DD'
                ,'Dp2KS0KpKpPim_KS0LL'
                ,'Dp2KS0KpKpPim_KS0DD'
                ,'Dp2KS0KmPipPip_KS0LL'
                ,'Dp2KS0KmKpPip_KS0LL'
                ,'Dp2KS0KmKpPip_KS0DD'
                ,'MuTag_DstpMu_Dstp2D0Pip_D02PimPip'
                ,'MuTag_DstpMu_Dstp2D0Pip_D02KpPim'
                ,'LcpToPpKpPim'
                ,'LcpToPpKmKp'
                ,'Lcp2LamKp_LamDD'
                ,'Dstp2D0Pip_D02KmKpPimPip'
                ,'Dstp2D0Pip_D02KmKmKpPip'
                ,'DspToKpKpPim'
                ,'Dsp2KmPimPipPipPip'
                ,'Dsp2KS0KpPimPip_KS0LL'
                ,'Dsp2KS0KmPipPip_KS0DD'
                ,'Dp2KS0KpPimPip_KS0LL'
                ,'Dp2KS0KmPipPip_KS0DD'
                ,'DpToKmPipPip'
                ,'Dstp2D0Pip_D02KpPim_LTUNB'
                ,'Dstp2D0Pip_D02PimPip_LTUNB'
                ,'DspToPimPipPip'
                ,'Dstp2D0Pip_D02KmPip_LTUNB'
                ,'DspToKpPimPip'
                ,'DpToPimPipPip'
                ,'Dstp2D0Pip_D02PimPimPipPip'
                ,'Dstp2D0Pip_D02KmPip'
                ,'Dstp2D0Pip_D02KmPimPipPip'
                ,'DpToKpPimPip'
                ,'Dp2KS0Pip_KS0LL'
                ,'Dstp2D0Pip_D02KmKp_LTUNB'
                ,'Dstp2D0Pip_D02KpPimPimPip'
                ,'Lcp2LamPip_LamDD'
                ,'DspToKmKpPip'
                ,'Dsp2KS0PimPipPip_KS0LL'
                ,'Dp2KS0Pip_KS0DD'
                ,'Dp2KS0PimPipPip_KS0LL'
                ,'Dp2KS0Kp_KS0DD'
                ] ]
            ,'CharmSpec' : [ 'Hlt2CharmHadSpec_' + k + 'Turbo' for k in [
                'DsPi'
                ,'D0ToK3Pi_Pr'
                ,'LcPr'
                ,'LcPi'
                ,'LcPi0'
                ,'LcKpm'
                ,'Dst_KPi_Pr'
                ,'Dst_K3Pi_Kpm'
                ,'LcLambda'
                ,'LcKs'
                ,'LcEta'
                ,'Dst_KPi_Lambda'
                ,'Dst_KPi_Ks'
                ,'Dst_KPi_Eta'
                ,'Dst_K3Pi_Lambda'
                ,'Dst_K3Pi_Ks'
                ,'Dst_K3Pi_Eta'
                ,'DsPr'
                ,'DsLambda'
                ,'DsKs'
                ,'DsEta'
                ,'DpLambda'
                ,'DpKs'
                ,'DpEta'
                ,'D0ToKPi_Lambda'
                ,'D0ToKPi_Ks'
                ,'D0ToKPi_Eta'
                ,'D0ToK3Pi_Lambda'
                ,'D0ToK3Pi_Ks'
                ,'D0ToK3Pi_Eta'
                ,'Dst_K3Pi_Pr'
                ,'DsKpm'
                ,'D0ToKPi_Pi0'
                ,'D0ToKPi_Pi'
                ,'DpPi0'
                ,'DpPi'
                ,'D0ToKPi_Kpm'
                ,'D0ToK3Pi_Pi0'
                ,'D0ToK3Pi_Pi'
                ,'DpKpm'
                ,'Dst_KPi_Pi'
                ,'Dst_KPi_Pi0'
                ,'D0ToKPi_Pr'
                ,'D0ToK3Pi_Kpm'
                ,'Dst_K3Pi_Pi'
                ,'Dst_K3Pi_Pi0'
                ,'Dst_KPi_Kpm'
                ,'DsPi0'
                ,'DpPr'
                ] ]
        },
        'v9r9b_0x00f4014d':
        {
            'PID' : [ 'Hlt2PID' + k + 'TurboCalib' for k in [
                                'Lambda2PPiLL',
                                'Lambda2PPiLLhighPT',
                                'Lambda2PPiLLveryhighPT',
                                'Lambda2PPiLLisMuon',
                                'Lambda2PPiDD',
                                'Lambda2PPiDDhighPT',
                                'Lambda2PPiDDveryhighPT',
                                'Lambda2PPiDDisMuon',
                                'Ks2PiPiLL',
                                'Ks2PiPiDD',
                                'DetJPsiEEL0PosTagged',
                                'DetJPsiEEL0NegTagged',
                                'B2KJPsiEEL0SSTagged',
                                'B2KJPsiEEL0OSTagged',
                                'DetJPsiMuMuPosTagged',
                                'DetJPsiMuMuNegTagged',
                                'B2KJPsiMuMuSSTagged',
                                'B2KJPsiMuMuOSTagged',
                                'DetPhiMuMuPosTagged',
                                'DetPhiMuMuNegTagged',
                                'Ds2PiPhiMuMuSSTagged',
                                'Ds2PiPhiMuMuOSTagged',
                                'DetJPsiEEPosTagged',
                                'DetJPsiEENegTagged',
                                'B2KJPsiEESSTagged',
                                'B2KJPsiEEOSTagged',
                                'Sc02LcPi',
                                'Scpp2LcPi',
                                'Lb2LcPi',
                                'Lb2LcMuNu',
                                'D02KPiPiPi',
                                'D02KPiPiPiTag',
                                'D02KPiTag'
                               ] 
                    ]
            ,'TrackCalib' : [
                'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalib'
                ]
            ,'DiMuon' : [ 
                'Hlt2DiMuonJPsiTurbo'
                ,'Hlt2JPsiReFitPVsTurbo'
                ,'Hlt2DiMuonPsi2STurbo'
                ,'Hlt2DiMuonBTurbo'
                ]
            ,'Charm' : [
                'Hlt2CharmHadDs2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDs2PiPiPi_XSecTurbo'
                ,'Hlt2CharmHadD02KPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadDpm2KPiPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi0_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadSigmac_2LcPi_XSecTurbo'
                ,'Hlt2CharmHadDpm2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2DsGamma_Ds2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Gamma_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi_D02K3Pi_XSecTurbo'
                ,'Hlt2CharmHadLc2KPPi_XSecTurbo'
                ,'Hlt2CharmHadLc2KPK_XSecTurbo'
                ,'Hlt2CharmHadLc2PiPPi_XSecTurbo'
                ,'Hlt2CharmHadXic02PKKPi_Turbo'
                ]
        },
        'v9r9_0x004004c':
        {
            'PID' : [ 'Hlt2PID' + k + 'TurboCalib' for k in [
                                'Lambda2PPiLL',
                                'Lambda2PPiLLhighPT',
                                'Lambda2PPiLLveryhighPT',
                                'Lambda2PPiLLisMuon',
                                'Lambda2PPiDD',
                                'Lambda2PPiDDhighPT',
                                'Lambda2PPiDDveryhighPT',
                                'Lambda2PPiDDisMuon',
                                'Ks2PiPiLL',
                                'Ks2PiPiDD',
                                'DetJPsiEEL0PosTagged',
                                'DetJPsiEEL0NegTagged',
                                'B2KJPsiEEL0SSTagged',
                                'B2KJPsiEEL0OSTagged',
                                'DetJPsiMuMuPosTagged',
                                'DetJPsiMuMuNegTagged',
                                'B2KJPsiMuMuSSTagged',
                                'B2KJPsiMuMuOSTagged',
                                'DetPhiMuMuPosTagged',
                                'DetPhiMuMuNegTagged',
                                'Ds2PiPhiMuMuSSTagged',
                                'Ds2PiPhiMuMuOSTagged',
                                'DetJPsiEEPosTagged',
                                'DetJPsiEENegTagged',
                                'B2KJPsiEESSTagged',
                                'B2KJPsiEEOSTagged',
                                'Sc02LcPi',
                                'Scpp2LcPi',
                                'Lb2LcPi',
                                'Lb2LcMuNu',
                                'D02KPiPiPi',
                                'D02KPiPiPiTag',
                                'D02KPiTag'
                               ] 
                    ]
            ,'TrackCalib' : [
                'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalib'
                ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalib'
                ]
            ,'DiMuon' : [ 
                'Hlt2DiMuonJPsiTurbo'
                ,'Hlt2JPsiReFitPVsTurbo'
                ,'Hlt2DiMuonPsi2STurbo'
                ,'Hlt2DiMuonBTurbo'
                ]
            ,'Charm' : [
                'Hlt2CharmHadDs2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDs2PiPiPi_XSecTurbo'
                ,'Hlt2CharmHadD02KPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadDpm2KPiPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi0_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadSigmac_2LcPi_XSecTurbo'
                ,'Hlt2CharmHadDpm2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2DsGamma_Ds2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Gamma_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi_D02K3Pi_XSecTurbo'
                ,'Hlt2CharmHadLc2KPPi_XSecTurbo'
                ,'Hlt2CharmHadLc2KPK_XSecTurbo'
                ,'Hlt2CharmHadLc2PiPPi_XSecTurbo'
                ]
        },
        'v9r8':  
        { 
            'PID' : [
                'Hlt2PIDDetJPsiMuMuPosTaggedTurbo'
                ,'Hlt2PIDB2KJPsiMuMuSSTaggedTurbo'
                ,'Hlt2PIDDs2PiPhiMuMuOSTaggedTurbo'
                ,'Hlt2PIDDetPhiMuMuPosTaggedTurbo'
                ,'Hlt2PIDB2KJPsiMuMuOSTaggedTurbo'
                ,'Hlt2PIDDetPhiMuMuNegTaggedTurbo'
                ,'Hlt2PIDLb2LcMuNuTurbo'
                ,'Hlt2PIDDs2PiPhiMuMuSSTaggedTurbo'
                ,'Hlt2PIDDetJPsiMuMuNegTaggedTurbo'
                ,'Hlt2PIDB2KJPsiEEOSTaggedTurbo'
                ,'Hlt2PIDB2KJPsiEESSTaggedTurbo'
                ,'Hlt2PIDDetJPsiEEPosTaggedTurbo'
                ,'Hlt2PIDDetJPsiEENegTaggedTurbo'
                ,'Hlt2PIDLb2LcPiTurbo'
                ,'Hlt2PIDScpp2LcPiTurbo'
                ,'Hlt2PIDLambda2PPiDDTurbo'
                ,'Hlt2PIDSc02LcPiTurbo'
                ,'Hlt2PIDLambda2PPiDDisMuonTurbo'
                ,'Hlt2PIDD02KPiPiPiTagTurbo'
                ,'Hlt2PIDKs2PiPiLLTurbo'
                ,'Hlt2PIDD02KPiTagTurbo'
                ,'Hlt2PIDLambda2PPiLLTurbo'
                ,'Hlt2PIDLambda2PPiLLisMuonTurbo'
                ,'Hlt2PIDD02KPiPiPiTurbo'
                ,'Hlt2PIDLambda2PPiDDhighPTurbo'
                ,'Hlt2PIDKs2PiPiDDTurbo'
                ,'Hlt2PIDLambda2PPiLLhighPTurbo'
                ,'Hlt2PIDB2KJPsiEEL0SSTaggedTurbo'
                ,'Hlt2PIDDetJPsiEEL0PosTaggedTurbo'
                ,'Hlt2PIDB2KJPsiEEL0OSTaggedTurb'
                ,'Hlt2PIDDetJPsiEEL0NegTaggedTurbo'
                ,'Hlt2PIDLambda2PPiLLveryhighPTurbo'
                ,'Hlt2PIDLambda2PPiDDveryhighPTurbo'
                ] 
            ,'DiMuon' : [ 
                'Hlt2DiMuonJPsiTurbo'
                ,'Hlt2DiMuonPsi2STurbo'
                ,'Hlt2DiMuonBTurbo'
                ,'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurbo'
                ,'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurbo'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurbo'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurbo'
                ,'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurbo'
                ,'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurbo'
                ,'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurbo'
                ,'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurbo'
                ,'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurbo'
                ,'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurbo'
                ,'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurbo'
                ,'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurbo'
                ]
            ,'Charm' : [
                'Hlt2CharmHadDs2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDs2PiPiPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadDpm2KPiPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi0_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadSigmac_2LcPi_XSecTurbo'
                ,'Hlt2CharmHadDpm2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2DsGamma_Ds2KKPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Gamma_D02KPi_XSecTurbo'
                ,'Hlt2CharmHadDst_2D0Pi_D02K3Pi_XSecTurbo'
                ,'Hlt2CharmHadLc2KPPi_XSecTurbo'
                ,'Hlt2CharmHadLc2KPK_XSecTurbo'
                ,'Hlt2CharmHadLc2PiPPi_XSecTurbo'
                ]
            }
    }

prescaleDict = { 
        4 : 
        {
            'CharmHad' : {
                'Hlt2CharmHadD02KmPipTurbo' : 0.1
                ,'Hlt2CharmHadXim2LamPim_DDDTurbo' : 0.1
                ,'Hlt2CharmHadDpToKmPipPipTurbo' : 0.1
                },
            'non-prescaled-PR' : [
                'Hlt2CharmHadDspToKmKpPipTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DDTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DD_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LLTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LL_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DDTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DD_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LLTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LL_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DDTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DD_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LLTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LL_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DDTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LLTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LL_LTUNBTurbo'
                , 'Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo'
                , 'Hlt2CharmHadLcpToPpKmPipTurbo'
                , 'Hlt2CharmHadOmm2LamKm_DDDTurbo'
                , 'Hlt2CharmHadOmm2LamKm_DDLTurbo'
                , 'Hlt2CharmHadOmm2LamKm_LLLTurbo'
                , 'Hlt2CharmHadXic0ToPpKmKmPipTurbo'
                , 'Hlt2CharmHadXicpToPpKmPipTurbo'
                , 'Hlt2CharmHadXim2LamPim_DDLTurbo'
                , 'Hlt2CharmHadXim2LamPim_LLLTurbo'
                , 'Hlt2DiMuonBTurbo'
                , 'Hlt2DiMuonJPsiTurbo'
                , 'Hlt2DiMuonPsi2STurbo'
                , 'Hlt2ExoticaDiMuonNoIPTurbo'
                , 'Hlt2ExoticaPrmptDiMuonSSTurbo'
                , 'Hlt2ExoticaPrmptDiMuonTurbo'
                , 'Hlt2TrackEff_D0ToKpiKaonProbeTurbo'
                , 'Hlt2TrackEff_D0ToKpiPionProbeTurbo'
                ]
        }
    }
