turbo_streams = {
    'DiMuon': {
        'lines': [
            'Hlt2DiMuonJPsiTurbo', 'Hlt2JPsiReFitPVsTurbo',
            'Hlt2DiMuonPsi2STurbo', 'Hlt2DiMuonBTurbo'
        ],
        'prescale':
        None
    },
        'CharmSpec': { 'lines' : [
            'Hlt2CharmHadSpec_' + k + 'Turbo' for k in [
                'DsPi', 'D0ToK3Pi_Pr', 'LcPr', 'LcPi', 'LcPi0', 'LcKpm',
                'Dst_KPi_Pr', 'Dst_K3Pi_Kpm', 'LcLambda', 'LcKs', 'LcEta',
                'Dst_KPi_Lambda', 'Dst_KPi_Ks', 'Dst_KPi_Eta', 'Dst_K3Pi_Lambda',
                'Dst_K3Pi_Ks', 'Dst_K3Pi_Eta', 'DsPr', 'DsLambda', 'DsKs', 'DsEta',
                'DpLambda', 'DpKs', 'DpEta', 'D0ToKPi_Lambda', 'D0ToKPi_Ks',
                'D0ToKPi_Eta', 'D0ToK3Pi_Lambda', 'D0ToK3Pi_Ks', 'D0ToK3Pi_Eta',
                'Dst_K3Pi_Pr', 'DsKpm', 'D0ToKPi_Pi0', 'D0ToKPi_Pi', 'DpPi0',
                'DpPi', 'D0ToKPi_Kpm', 'D0ToK3Pi_Pi0', 'D0ToK3Pi_Pi', 'DpKpm',
                'Dst_KPi_Pi', 'Dst_KPi_Pi0', 'D0ToKPi_Pr', 'D0ToK3Pi_Kpm',
                'Dst_K3Pi_Pi', 'Dst_K3Pi_Pi0', 'Dst_KPi_Kpm', 'DsPi0',
                'DpPr', 'D0ToK3Pi_Gamma', 'D0ToKPi_Gamma', 'DpGamma', 'DsGamma', 
                'Dst_K3Pi_Gamma', 'Dst_KPi_Gamma', 'LcGamma'
            ]
        ]},
        'Charm': { 'lines' : [
            'Hlt2CharmHad' + k + 'Turbo' for k in [
                'Dstp2D0Pip_D02PimPip', 'Dstp2D0Pip_D02KpPim',
                'Dstp2D0Pip_D02KmKp', 'LcpToPpPimPip', 'LcpToPpKmPip',
                'Lcp2LamKp_LamLL', 'Dsp2KS0PimPipPip_KS0DD', 'DpToKmKpPip',
                'Dp2KS0PimPipPip_KS0DD', 'Xic0ToPpKmKmPip_LTUNB',
                'Xic0ToPpKmKmPip', 'PentaToPhiPpPim',
                'MuTag_DstpMu_Dstp2D0Pip_D02KmPip',
                'MuTag_DstpMu_Dstp2D0Pip_D02KmKp', 'LcpToPpKmPip_LTUNB',
                'LcpToPpKmKpPimPip', 'Lcp2LamPip_LamLL',
                'Lcp2LamKmPipPip_Lam2PpPim', 'Lcp2LamKmKpPip_Lam2PpPim',
                'Dstp2D0Pip_D02KmKpKpPim', 'Dstp2D0Pip_D02KS0KS0_KS0LL_KS0DD',
                'Dstp2D0Pip_D02KS0KS0_KS0LL', 'Dstp2D0Pip_D02KS0KS0_KS0DD',
                'DspToKmKpPip_LTUNB', 'DspToKmKpKp', 'Dsp2KmKpPimPipPip',
                'Dsp2KmKmKpPipPip', 'Dsp2KS0KpPimPip_KS0DD',
                'Dsp2KS0KpKpPim_KS0LL', 'Dsp2KS0KpKpPim_KS0DD',
                'Dsp2KS0KmPipPip_KS0LL', 'Dsp2KS0KmKpPip_KS0LL',
                'Dsp2KS0KmKpPip_KS0DD', 'DpToKmPipPip_LTUNB', 'DpToKmKpKp',
                'Dp2KmPimPipPipPip', 'Dp2KmKpPimPipPip', 'Dp2KmKmKpPipPip',
                'Dp2KS0Kp_KS0LL', 'Dp2KS0KpPimPip_KS0DD', 'Dp2KS0KpKpPim_KS0LL',
                'Dp2KS0KpKpPim_KS0DD', 'Dp2KS0KmPipPip_KS0LL',
                'Dp2KS0KmKpPip_KS0LL', 'Dp2KS0KmKpPip_KS0DD',
                'MuTag_DstpMu_Dstp2D0Pip_D02PimPip',
                'MuTag_DstpMu_Dstp2D0Pip_D02KpPim', 'LcpToPpKpPim', 'LcpToPpKmKp',
                'Lcp2LamKp_LamDD', 'Dstp2D0Pip_D02KmKpPimPip',
                'Dstp2D0Pip_D02KmKmKpPip', 'DspToKpKpPim', 'Dsp2KmPimPipPipPip',
                'Dsp2KS0KpPimPip_KS0LL', 'Dsp2KS0KmPipPip_KS0DD',
                'Dp2KS0KpPimPip_KS0LL', 'Dp2KS0KmPipPip_KS0DD', 'DpToKmPipPip',
                'Dstp2D0Pip_D02KpPim_LTUNB', 'Dstp2D0Pip_D02PimPip_LTUNB',
                'DspToPimPipPip', 'Dstp2D0Pip_D02KmPip_LTUNB', 'DspToKpPimPip',
                'DpToPimPipPip', 'Dstp2D0Pip_D02PimPimPipPip',
                'Dstp2D0Pip_D02KmPip', 'Dstp2D0Pip_D02KmPimPipPip', 'DpToKpPimPip',
                'Dp2KS0Pip_KS0LL', 'Dstp2D0Pip_D02KmKp_LTUNB',
                'Dstp2D0Pip_D02KpPimPimPip', 'Lcp2LamPip_LamDD', 'DspToKmKpPip',
                'Dsp2KS0PimPipPip_KS0LL', 'Dp2KS0Pip_KS0DD',
                'Dp2KS0PimPipPip_KS0LL', 'Dp2KS0Kp_KS0DD',
                'DpToKpKpPim', 'DspToKmPipPip', 'LcpToPpKmKmPipPip',
                'LcpToPpKmPimPipPip', 'LcpToPpKpKpPimPim', 'LcpToPpKpPimPimPip',
                'LcpToPpPimPimPipPip', 'MuTag_Dstp_D02KmKmKpPip',
                'MuTag_Dstp_D02KmKpKpPim', 'MuTag_Dstp_D02KmKpPimPip',
                'MuTag_Dstp_D02KmPimPipPip', 'MuTag_Dstp_D02KpPimPimPip',
                'MuTag_Dstp_D02PimPimPipPip', 'Xiccp2D0PpKmPim_D02KmPip',
                'Xiccp2D0PpKmPip_D02KmPip', 'Xiccp2D0PpKpPim_D02KmPip',
                'Xiccp2DpPpKm_Dp2KmPipPip', 'Xiccp2DpPpKp_Dp2KmPipPip',
                'Xiccp2LcpKmPim_Lcp2PpKmPip', 'Xiccp2LcpKmPip_Lcp2PpKmPip',
                'Xiccp2LcpKpPim_Lcp2PpKmPip', 'Xiccp2Xic0Pim_Xic0ToPpKmKmPip',
                'Xiccp2Xic0Pip_Xic0ToPpKmKmPip', 'Xiccp2XicpPimPim_Xicp2PpKmPip',
                'Xiccp2XicpPimPip_Xicp2PpKmPip', 'Xiccpp2D0PpKmPimPip_D02KmPip',
                'Xiccpp2D0PpKmPipPip_D02KmPip', 'Xiccpp2D0PpKpPimPip_D02KmPip',
                'Xiccpp2DpPpKmPim_Dp2KmPipPip', 'Xiccpp2DpPpKmPip_Dp2KmPipPip',
                'Xiccpp2DpPpKpPip_Dp2KmPipPip', 'Xiccpp2LcpKmPimPip_Lcp2PpKmPip',
                'Xiccpp2LcpKmPipPip_Lcp2PpKmPip', 'Xiccpp2LcpKpPimPip_Lcp2PpKmPip',
                'Xiccpp2Xic0PimPip_Xic0ToPpKmKmPip', 'Xiccpp2Xic0PipPip_Xic0ToPpKmKmPip',
                'Xiccpp2XicpPim_Xicp2PpKmPip', 'Xiccpp2XicpPip_Xicp2PpKmPip'
            ]
        ]
  }
}

turcal_streams = { 
        'PID': { 'lines' : [
            'Hlt2PID' + k + 'TurboCalib' for k in [
                'DetPhiKKUnbiased', 'B2KJPsiMuMuPosTagged', 'B2KJPsiPPNegTagged',
                'DetJPsiMuMuNegTagged', 'Lambda2PPiDD', 'Ks2PiPiLL',
                'Ds2PiPhiKKUnbiased', 'Lb2LcMuNu', 'Lambda2PPiLLveryhighPT',
                'Ds2PiPhiKKNegTagged', 'DetPhiMuMuNegTagged', 'Lambda2PPiLL',
                'B2KJPsiEENegTagged', 'Lambda2PPiDDhighPT', 'Lb2LcPi',
                'B2KJPsiEEL0NegTagged', 'Lambda2PPiDDisMuon',
                'DetJPsiMuMuPosTagged', 'DetJPsiEEL0NegTagged',
                'Lambda2PPiLLisMuon', 'B2KJPsiEEPosTagged', 'B2KJPsiPPPosTagged',
                'Ks2PiPiDD', 'D02KPiTag', 'Ds2PiPhiMuMuPosTagged',
                'B2KJPsiEEL0PosTagged', 'DetJPsiEEL0PosTagged',
                'DetPhiKKNegTagged', 'Lambda2PPiDDveryhighPT',
                'Ds2PiPhiKKPosTagged', 'Scpp2LcPi', 'DetJPsiEENegTagged',
                'DetJPsiPPPosTagged', 'D02KPiPiPiTag', 'D02KPiPiPi',
                'DetPhiMuMuPosTagged', 'DetJPsiPPNegTagged', 'DetPhiKKPosTagged',
                'Ds2PiPhiMuMuNegTagged', 'Lambda2PPiLLhighPT',
                'DetJPsiEEPosTagged', 'B2KJPsiMuMuNegTagged', 'Sc02LcPi'
            ]
        ] },

        'TrackCalib': { 'lines' : [
            'Hlt2TrackEffDiMuonMuonTTPlusTaggedTurboCalib',
            'Hlt2TrackEffDiMuonMuonTTMinusTaggedTurboCalib',
            'Hlt2TrackEffDiMuonVeloMuonPlusTaggedTurboCalib',
            'Hlt2TrackEffDiMuonVeloMuonMinusTaggedTurboCalib',
            'Hlt2TrackEffDiMuonDownstreamPlusTaggedTurboCalib',
            'Hlt2TrackEffDiMuonDownstreamMinusTaggedTurboCalib',
            'Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalib',
            'Hlt2TrackEffDiMuonMuonTTMinusMatchedTurboCalib',
            'Hlt2TrackEffDiMuonVeloMuonPlusMatchedTurboCalib',
            'Hlt2TrackEffDiMuonVeloMuonMinusMatchedTurboCalib',
            'Hlt2TrackEffDiMuonDownstreamPlusMatchedTurboCalib',
            'Hlt2TrackEffDiMuonDownstreamMinusMatchedTurboCalib'
        ]}
}
